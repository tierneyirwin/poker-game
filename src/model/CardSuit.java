package model;

public enum CardSuit 
{
	SPADES("Spades"), 
	DIAMONDS("Diamonds"), 
	HEARTS("Hearts"),
	CLUBS("Clubs");
	private String suit;
	
	private CardSuit(String name)
	{
		suit = name;
	};
	
	public String getSuit()
	{
		return null;
	}

}
