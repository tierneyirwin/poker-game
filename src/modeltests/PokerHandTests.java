package modeltests;

import static org.junit.Assert.*;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardType;
import model.Hand;
import model.PokerHand;
import model.PokerHandRanking;

/**
 * PokerHand Test cases to check the functionality 
 * of the PokerHand Class
 * Due: 24 February 2016
 * @author Tierney Irwin
 *
 */
public class PokerHandTests 
{
	Image image;
	Hand hand;

	@Before
	public void setUp() 
	{
		hand = new Hand(5);
	}

	/**
	 * Tests if boolean isRoyalFlush is working properly and if determineRanking can
	 * work for a royal flush.
	 */
	@Test
	public void testIfRoyalFlush()
	{
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Spades",CardType.KING,image);
		Card card3 = new Card("Spades",CardType.QUEEN,image);
		Card card4 = new Card("Spades",CardType.JACK,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Royal Flush ranking",rank==PokerHandRanking.ROYAL_FLUSH.getRank());
		boolean valid = hand.isRoyalFlush();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isStraightFlush is working properly and if determineRanking can
	 * work for a straight flush.
	 */
	@Test
	public void testIfStraightFlush()
	{
		Card card1 = new Card("Spades", CardType.NINE,image);
		Card card2 = new Card("Spades",CardType.KING,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Spades",CardType.JACK,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Straight Flush ranking",rank==PokerHandRanking.STRAIGHT_FLUSH.getRank());
		boolean valid = hand.isStraightFlush();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isFourOfKind is working properly and if determineRanking can
	 * work for a four of a kind.
	 */
	@Test
	public void testIfFourOfKind()
	{
		Card card1 = new Card("Spades", CardType.FOUR,image);
		Card card2 = new Card("Hearts",CardType.FOUR,image);
		Card card3 = new Card("Clubs", CardType.FOUR,image);
		Card card4 = new Card("Diamonds",CardType.FOUR,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Four of a Kind ranking",rank==PokerHandRanking.FOUR_OF_KIND.getRank());
		boolean valid = hand.isFourOfKind();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isFullHouse is working properly and if determineRanking can
	 * work for a full house.
	 */
	@Test
	public void testIfFullHouse()
	{
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Hearts",CardType.ACE,image);
		Card card3 = new Card("Clubs", CardType.ACE,image);
		Card card4 = new Card("Diamonds",CardType.JACK,image);
		Card card5 = new Card("Spades",CardType.JACK,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Full House ranking",rank==PokerHandRanking.FULL_HOUSE.getRank());
		boolean valid = hand.isFullHouse();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isFlush is working properly and if determineRanking can
	 * work for a flush.
	 */
	@Test
	public void testIfFlush()
	{
		
		Card card1 = new Card("Spades", CardType.TEN,image);
		Card card2 = new Card("Spades",CardType.THREE,image);
		Card card3 = new Card("Spades", CardType.SIX,image);
		Card card4 = new Card("Spades",CardType.JACK,image);
		Card card5 = new Card("Spades",CardType.ACE,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Flush ranking",rank==PokerHandRanking.FLUSH.getRank());
		boolean valid = hand.isFlush();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isStraight is working properly and if determineRanking can
	 * work for a straight.
	 */
	@Test
	public void testIfStraight()
	{
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Hearts",CardType.TWO,image);
		Card card3 = new Card("Diamonds", CardType.THREE,image);
		Card card4 = new Card("Spades",CardType.FOUR,image);
		Card card5 = new Card("Clubs",CardType.FIVE,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Straight ranking",rank==PokerHandRanking.STRAIGHT.getRank());
		boolean valid = hand.isStraight();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isThreeOfKind is working properly and if determineRanking can
	 * work for a three of a kind.
	 */
	@Test
	public void testIfThreeOfKind()
	{
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Hearts",CardType.ACE,image);
		Card card3 = new Card("Diamonds", CardType.ACE,image);
		Card card4 = new Card("Spades",CardType.JACK,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Three of a Kind ranking",rank==PokerHandRanking.THREE_OF_KIND.getRank());
		boolean valid = hand.isThreeOfKind();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isTwoPair is working properly and if determineRanking can
	 * work for a two pair.
	 */
	@Test
	public void testIfTwoPair()
	{
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Diamonds",CardType.ACE,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Clubs",CardType.QUEEN,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Two Pair ranking",rank==PokerHandRanking.TWO_PAIR.getRank());
		boolean valid = hand.isTwoPair();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isPair is working properly and if determineRanking can
	 * work for a pair.
	 */
	@Test
	public void testIfPair()
	{
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Clubs",CardType.ACE,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Hearts",CardType.FIVE,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal Pair ranking",rank==PokerHandRanking.PAIR.getRank());
		boolean valid = hand.isPair();
		assertTrue("Should be true",valid);
	}
	
	/**
	 * Tests if boolean isHighCard is working properly and if determineRanking can
	 * work for a high card.
	 */
	@Test
	public void testIfHighCard()
	{
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Hearts",CardType.FIVE,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Diamonds",CardType.SIX,image);
		Card card5 = new Card("Clubs",CardType.TWO,image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		int rank = hand.determineRanking();
		assertTrue("Should equal High Card ranking",rank==PokerHandRanking.HIGH_CARD.getRank());
		boolean valid = hand.isHighCard();
		assertTrue("Should be true",valid);
	}
}
