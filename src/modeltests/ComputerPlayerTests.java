package modeltests;

import static org.junit.Assert.*;

import java.awt.Image;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardType;
import model.ComputerPlayer;
import model.Hand;
import model.Player;

/**
 * ComputerPlayer Test cases to check 
 * the functionality of the ComputerPlayer Class
 * Due: 24 February 2016
 * @author Tierney Irwin
 *
 */
public class ComputerPlayerTests 
{
	ComputerPlayer computerPlayer;
	Hand hand;
	Image image;

	@Before
	public void setUp() 
	{
		computerPlayer = new ComputerPlayer("AI");
		hand = computerPlayer.getHand();
	}

	/**
	 * Tests if Computer Player can determine what cards to discard 
	 * if they have straight to royal flush ranking.
	 */
	@Test
	public void testIfDiscardsForStraightPlus() 
	{

		int rank = hand.determineRanking();
		if (rank > 4) 
		{
			Vector <Integer> discard = computerPlayer.selectCardsToDiscard();
			assertTrue("No cards should be discarded", discard.contains(null));
		}
	}
	
	/**
	 * Tests if computer player can discard correct cards if
	 * they have three of a kind ranking.
	 */

	@Test
	public void testIfThreeOfKindDiscard() 
	{
		Card card1 = new Card("Hearts", CardType.ACE, image);
		Card card2 = new Card("Diamonds", CardType.ACE, image);
		Card card3 = new Card("Clubs", CardType.ACE, image);
		Card card4 = new Card("Hearts", CardType.TWO, image);
		Card card5 = new Card("Hearts", CardType.NINE, image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		Vector<Integer> deleted = computerPlayer.selectCardsToDiscard();
		assertTrue("Should have deleted two and nine", deleted.contains(card4) && deleted.contains(card5));
	}

	/**
	 * Tests if computer player can discard correct cards if
	 * they have two pair ranking.
	 */
	@Test
	public void testIfTwoPairDeleted() 
	{
		Card card1 = new Card("Hearts", CardType.ACE, image);
		Card card2 = new Card("Diamonds", CardType.ACE, image);
		Card card3 = new Card("Clubs", CardType.FOUR, image);
		Card card4 = new Card("Hearts", CardType.FOUR, image);
		Card card5 = new Card("Hearts", CardType.NINE, image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		Vector<Integer> deleted = computerPlayer.selectCardsToDiscard();
		assertTrue("Should have deleted the non paired card", deleted.contains(card5));
	}

	/**
	 * Tests if computer player can discard correct cards if
	 * they have pair ranking.
	 */
	@Test
	public void testIfPairDeleted() 
	{
		Card card1 = new Card("Hearts", CardType.ACE, image);
		Card card2 = new Card("Diamonds", CardType.ACE, image);
		Card card3 = new Card("Clubs", CardType.FIVE, image);
		Card card4 = new Card("Hearts", CardType.FOUR, image);
		Card card5 = new Card("Hearts", CardType.NINE, image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		Vector<Integer> deleted = computerPlayer.selectCardsToDiscard();
		assertTrue("Should have deleted the non paired cards",
				deleted.contains(card5) && deleted.contains(card4) && deleted.contains(card3));
	}

	/**
	 * Tests if computer player can discard correct cards if
	 * they have high card ranking.
	 */
	@Test
	public void testIfHighCardDeleted() 
	{
		Card card1 = new Card("Hearts", CardType.ACE, image);
		Card card2 = new Card("Diamonds", CardType.KING, image);
		Card card3 = new Card("Clubs", CardType.FOUR, image);
		Card card4 = new Card("Hearts", CardType.FOUR, image);
		Card card5 = new Card("Hearts", CardType.NINE, image);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		Vector<Integer> deleted = computerPlayer.selectCardsToDiscard();
		assertTrue("Should have deleted three lowest cards",
				deleted.contains(card5) && deleted.contains(card4) && deleted.contains(card3));
	}
}
