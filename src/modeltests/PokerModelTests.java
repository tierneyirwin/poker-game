package modeltests;

import static org.junit.Assert.*;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardType;
import model.ComputerPlayer;
import model.Hand;
import model.Player;
import model.PokerModel;

/**
 * PokerModel Test cases to check the functionality 
 * of the PokerModel Class
 * Due: 24 February 2016
 * @author Tierney Irwin
 *
 */
public class PokerModelTests 
{
	private PokerModel game;
	private Player player1 = new Player("Player");
	Image image;

	@Before
	public void setUp() 
	{
		game = new PokerModel(player1);

	}

	/**
	 * Tests if dealCards() can properly deal hands out to computer and player.
	 */
	@Test
	public void dealCardsProperAmount() 
	{
		Player player = new Player("Hilario");
		//PokerModel myPokerModel = new PokerModel(player);
		game.resetGame();
		game.dealCards();
		for (int i = 0; i < 2; i++) {
			player = PokerModel.getPlayer(i);
			Hand hand = player.getHand();
			int numCards = hand.getNumberCardsInHand();
			assertTrue("Wrong number of cards in Hand", numCards == 5);
		}
	}
	
	/**
	 * Tests if determineWinner() functions correctly.
	 */
	@Test
	public void testifIncorrectDetermineWinner() 
	{
		Hand hand1 = new Hand(5);
		hand1 = player1.getHand();
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Spades",CardType.KING,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Spades",CardType.JACK,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		hand1.add(card1);
		hand1.add(card2);
		hand1.add(card3);
		hand1.add(card4);
		hand1.add(card5);
		Hand hand2 = PokerModel.getPlayer(1).getHand();
		Card card6 = new Card("Spades", CardType.TWO,image);
		Card card7 = new Card("Spades", CardType.THREE,image);
		Card card8 = new Card("Spades", CardType.FOUR,image);
		Card card9 = new Card("Spades", CardType.FIVE,image);
		Card card0 = new Card("Spades", CardType.TWO,image);
		hand2.add(card6);
		hand2.add(card7);
		hand2.add(card8);
		hand2.add(card9);
		hand2.add(card0);
		String winner = game.determineWinner().getName();
		assertTrue("Player should have won",winner==player1.getName());
	}

	/**
	 * Tests if switchTurns() can switch between computer and player's turns.
	 */
	@Test
	public void testIfSwitchTurnsCorrect() 
	{
		/**
		 * Starts new game and deals cards to each. Checks if Player[0] is
		 * first.
		 */
		game.resetGame();
		game.dealCards();
		assertTrue("Should be Player's turn", game.getIndexPlayerUp() == 0);
		assertTrue("Should be Player's turn", game.getPlayerUp().getName() == player1.getName());
		/**
		 * Checks if switchTurns() changes index to computerPlayer
		 */
		game.switchTurns();
		assertTrue("Should be Computer's turn", game.getIndexPlayerUp() == 1);
	}

	/**
	 * Tests if resetGame() clears all hands.
	 */
	@Test
	public void testIfResetsCorrectly() 
	{
		game.dealCards();
		game.resetGame();
		Hand hand = player1.getHand();
		int numCards = hand.getNumberCardsInHand();
		assertTrue("Player hand should be empty",numCards==0);

	}
}
