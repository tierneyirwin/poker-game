package modeltests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Player;

/**
 * Player Test cases to check the functionality 
 * of the Player Class
 * Due: 24 February 2016
 * @author Tierney Irwin
 *
 */
public class PlayerTests 
{
	Player player;
	Player player2;

	@Before
	public void setUp()  
	{
		player = new Player("WadeWilson");
		player2 = new Player("Stev3");
	}

	/**
	 * Tests if validateName passes incorrect names.
	 */
	@Test
	public void testifInvalidName()
	{
		boolean valid = player.validateName("WadeWilson");
		assertTrue("Name is good",valid);
		
		
		valid = player2.validateName("Stev3");
		assertFalse("Name is invalid",valid);
	}
	
	/**
	 * Tests if incrementNumberWins() functions correctly.
	 */
	@Test
	public void testIfCorrectIncrementing()
	{
		int initial = player.getNumberWins();
		int change = player.incrementNumberWins();
		assertFalse("Should have changed player's wins",change==initial);
	}
}
