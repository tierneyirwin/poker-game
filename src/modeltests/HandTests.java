package modeltests;

import static org.junit.Assert.*;

import java.awt.Image;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardType;
import model.Hand;

/**
 * Hand Test cases to check the functionality 
 * of the Hand Class
 * Due: 24 February 2016
 * @author Tierney Irwin
 *
 */
public class HandTests 
{
	Image image;

	@Before
	public void setUp()  
	{
		
	}

	/**
	 * Tests if orderCards() correctly orders a hand.
	 */
	@Test
	public void testifIncorrectOrdering()
	{
		Hand hand = new Hand(5);
		Hand hand2 = new Hand(5);
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Spades",CardType.KING,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Spades",CardType.JACK,image);
		Card card5 = new Card("Spades",CardType.TEN,image);
		
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		/**
		 * Checks if orderCards() doesn't 
		 * change an already ordered hand.
		 */
		hand.orderCards();
		boolean valid=true;
		for(int i = 0;i<hand.getNumberCardsInHand()-1;i++)
		{
			if(hand.getCards().get(i).getType()>hand.getCards().get(i+1).getType())
			{
				valid=false;
				break;
			}
		}
		assertTrue("Should be same order",valid);
		
		Card card6 = new Card("Spades", CardType.FIVE,image);
		Card card7 = new Card("Clubs",CardType.ACE,image);
		Card card8 = new Card("Spades", CardType.QUEEN,image);
		Card card9 = new Card("Hearts",CardType.FIVE,image);
		Card card0 = new Card("Spades",CardType.TWO,image);
		
		hand2.add(card6);
		hand2.add(card7);
		hand2.add(card8);
		hand2.add(card9);
		hand2.add(card0);
		/**
		 * Checks if orderCards() correctly orders the hand
		 */
		hand2.orderCards();
		valid=true;
		for(int i = 0;i<hand.getNumberCardsInHand()-1;i++)
		{
			if(hand.getCards().get(i).getType()>hand.getCards().get(i+1).getType())
			{
				valid=false;
				break;
			}
		}
		assertTrue("Should be correct order",valid);
	}
	
	/**
	 * Tests if discard() correctly rids of card chosen.
	 */
	@Test
	public void testIfCorrectDiscard()
	{
		Hand hand = new Hand(5);
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Spades",CardType.KING,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Spades",CardType.FIVE,image);
		Card card5 = new Card("Spades",CardType.TWO,image);
		
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		
		Vector<Integer> v = new Vector<Integer>();
		v.add(3);
		v.add(4);
		Vector <Card> discard = hand.discard(v);
		assertFalse("Should have gotten rid "
				+ "of last two cards in hand",discard.contains(card4)&&discard.contains(card5));
	}
	
	/**
	 * Tests if add function can add more than should be in hand.
	 */
	@Test
	public void testIfIncorrectAddCards()
	{
		Hand hand = new Hand(5);
		
		Card card1 = new Card("Spades", CardType.ACE,image);
		Card card2 = new Card("Spades",CardType.KING,image);
		Card card3 = new Card("Spades", CardType.QUEEN,image);
		Card card4 = new Card("Spades",CardType.FIVE,image);
		Card card5 = new Card("Spades",CardType.TWO,image);
		Card card6 = new Card("Hearts",CardType.THREE,image);
		
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		hand.add(card4);
		hand.add(card5);
		hand.add(card6);
		assertFalse("Hand should not have added the last card.",hand.getNumberCardsInHand()!=hand.getMaxNumberCards());
		
	}

}
