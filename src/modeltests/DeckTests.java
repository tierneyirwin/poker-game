package modeltests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.Deck;

/**
 * Deck Test cases to check the 
 * functionality of the Deck Class
 * Due: 24 February 2016
 * @author Tierney Irwin
 *
 */
public class DeckTests {

	Deck deck;
	@Before
	public void setUp()  
	{
		deck = new Deck();
		deck.constructDeck();
	}
	
	/**
	 * Tests if Cards properly different order in deck.
	 */
	@Test
	public void testIfShuffle()
	{
		boolean valid = deck.shuffle();
		assertTrue("Deck should be shuffled",valid);
	}
	
	/**
	 * Tests if draw() produces a Card that isn't null.
	 */
	@Test
	public void testIfDrawCard()
	{
		Card drawn = deck.draw();
		assertNotNull("Should have drawn a card",drawn);
	}
}
