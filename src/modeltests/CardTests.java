package modeltests;

import static org.junit.Assert.*;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardType;
import model.Hand;
import model.PokerHand;

/**
 * Card Test cases to check the functionality of the Card Class
 * Due: 24 February 2016
 * @author Tierney Irwin
 *
 */
public class CardTests 
{
	Image image;
	Card myCard;

	@Before
	public void setUp() 
	{
		myCard = new Card("Hearts",CardType.ACE,image);
		
	}
/**
 * Tests if flip() correctly changed the image of a card.
 */
	@Test
	public void testIfFaceCorrectly()
	{
		boolean first = myCard.isFaceUp();
		myCard.flip();
		boolean last = myCard.isFaceUp();
		assertTrue("Should have flipped the card.",first!=last);
	}

}
